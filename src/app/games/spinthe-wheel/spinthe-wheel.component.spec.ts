import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SpintheWheelComponent } from './spinthe-wheel.component';

describe('SpintheWheelComponent', () => {
  let component: SpintheWheelComponent;
  let fixture: ComponentFixture<SpintheWheelComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SpintheWheelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpintheWheelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
