import { Component, OnInit ,ViewEncapsulation} from '@angular/core';
import { NgForm } from '@angular/forms';
import { HttpServiceService } from 'src/app/Services/common/http-service.service';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Title, Meta } from '@angular/platform-browser';
import { ExtraActivitiesComponent } from 'src/app/global/extra-activities/extra-activities.component';
import { Router } from '@angular/router';

interface Categories {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ContactUsComponent implements OnInit {
  title = 'Online Surveys For Money - Opinionest';
  description='Looking for online surveys that pay cash ? You have landed at the right place! Contact us for more details.';
  keywords='online surveys, pay cash,contact us.';



  formSubmit: boolean=false;
  contactUs: boolean = true;
  selectedItem: string;
  emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
  isValidFormSubmitted = false;  
  referenceNumber: string;
  userName: string;
  emailAddress: string;
  isEnterprise: boolean;
  isExistingUser: boolean = true;
  showForm: boolean = false;
  nextPressed: boolean = false;
  contactRaio: string;
  category: Categories[] = [
    {value: '0', viewValue: 'Offer & Rewards'},
    {value: '1', viewValue: 'Referrals'},
    {value: '2', viewValue: 'Report a bug'},
    {value: '2', viewValue: 'Privacy/GDPR Request'},
    {value: '2', viewValue: 'Others'},
  ];
  disableButton: string='Submit';
  constructor(public _HttpServiceService: HttpServiceService,public _snackBar: MatSnackBar,
    private titleService: Title,private meta: Meta,private seoService:ExtraActivitiesComponent, public _router: Router) { 

  }
  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }
  ngOnInit() {
    this.contactUs = true;
    this.formSubmit = false;
    this.titleService.setTitle(this.title);
    this.meta.addTag({name: 'keywords', content: this.keywords});
    this.meta.addTag({name: 'description', content: this.description});
    this.meta.addTag({name: 'og:title', content: this.title});
    this.meta.addTag({name: 'og:site_name', content: 'Opinionest'});
    this.meta.addTag({name: 'og:url', content: 'https://opinionest.com/'});
    this.meta.addTag({name: 'og:description', content:this.description});
    this.meta.addTag({name: 'og:type', content: 'website'});
    this.meta.addTag({name: 'og:image', content: 'https://opinionest.com/assets/images/logo-color.png'});
    this.seoService.createLinkForCanonicalURL();

  }

  onSubmit(f: NgForm) {
    if (f.invalid) {  
      return;  
    }  
    this.disableButton='Please wait';
    this.isValidFormSubmitted = false;
    const url = '/api/Home/ContactFormSubmission';
    let body = {
      "contactName": f.value.name,
      "contactEmail": f.value.email,
      "categoryId": f.value.selectedItem,
      "query": f.value.query,
      "contactNo": f.value.contactnumber == undefined ? 0 :  f.value.contactnumber
    }
    this._HttpServiceService.PostWithoutToken(url, body).subscribe(response => {
     debugger;
      this.disableButton='Submit';
      
      if (response.code === 200) {
        this.openSnackBar(response.message, "Succesfull");
        this.referenceNumber = response.data;
        this.contactUs = false;
        this.formSubmit = true;
        f.resetForm();
        localStorage.setItem("referenceNumber",  this.referenceNumber.toString());
        localStorage.setItem("isExistingUser",  this.isExistingUser.toString());
        this._router.navigate(['contact-us/thank-you/']);
 
      }
      else{
        this.disableButton='Submit';
      }
      // if (response.message === "Success") {
      //   this.openSnackBar("Record saved successfully", "Succesfull");
      // }
    });
  }

  getProfile(){
    if (this._HttpServiceService.isLoggedIn()) {
      const url = '/api/User/GetProfile';

      this._HttpServiceService.Get(url)
        .subscribe(
          (response) => {
            if (response) {
              this.userName = response.data.fullName;
              this.emailAddress = response.data.emailAddress;
              console.log(this.userName);
              console.log(this.emailAddress);
            }
          },
          err => {

          });
    }
  }
  handleChange(eventPressed: any){
    if(eventPressed.value == 1){
      this.isExistingUser = true;
      this.isEnterprise = false;
    }
    if(eventPressed.value == 2){
      this.isEnterprise = true;
      this.isExistingUser = false;
    }
  }

  onNext(){
    this.nextPressed = true;
    this.showForm = true; 
    this.getProfile();
  }

  goBack(){
    this.nextPressed = false;
    this.showForm = false; 
  }
}
