import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Title, Meta } from '@angular/platform-browser';
import { ExtraActivitiesComponent } from 'src/app/global/extra-activities/extra-activities.component';

@Component({
  selector: 'app-how-it-works',
  templateUrl: './how-it-works.component.html',
  styleUrls: ['./how-it-works.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HowItWorksComponent implements OnInit {
  title = 'Make Easy Money Online | Earn Money Through Surveys - Opinionest';
  description='Looking for additional money, Opinionest is a leading paid survey site for market researchers to make money quickly for giving their opinion online.';
  keywords='Additional money,paid survey site,make money easily,opinion online.';

  constructor(private titleService: Title,private meta: Meta,private seoService:ExtraActivitiesComponent) { }

  ngOnInit() {
    this.titleService.setTitle(this.title);
    this.meta.addTag({name: 'keywords', content: this.keywords});
    this.meta.addTag({name: 'description', content: this.description});
    this.meta.addTag({name: 'og:title', content: this.title});
    this.meta.addTag({name: 'og:site_name', content: 'Opinionest'});
    this.meta.addTag({name: 'og:url', content: 'https://opinionest.com/'});
    this.meta.addTag({name: 'og:description', content:this.description});
    this.meta.addTag({name: 'og:type', content: 'website'});
    this.meta.addTag({name: 'og:image', content: 'https://opinionest.com/assets/images/logo-color.png'});
    this.seoService.createLinkForCanonicalURL();
  }

}
