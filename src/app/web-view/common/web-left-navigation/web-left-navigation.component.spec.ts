import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { WebLeftNavigationComponent } from './web-left-navigation.component';

describe('WebLeftNavigationComponent', () => {
  let component: WebLeftNavigationComponent;
  let fixture: ComponentFixture<WebLeftNavigationComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ WebLeftNavigationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebLeftNavigationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
