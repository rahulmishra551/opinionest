import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { HeaderWebComponent } from './header-web.component';

describe('HeaderWebComponent', () => {
  let component: HeaderWebComponent;
  let fixture: ComponentFixture<HeaderWebComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderWebComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderWebComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
