import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TermsofServicesComponent } from './termsof-services.component';

describe('TermsofServicesComponent', () => {
  let component: TermsofServicesComponent;
  let fixture: ComponentFixture<TermsofServicesComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ TermsofServicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TermsofServicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
