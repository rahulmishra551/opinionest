import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ExtraActivitiesComponent } from 'src/app/global/extra-activities/extra-activities.component';
import { Title, Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-cookies-policy',
  templateUrl: './cookies-policy.component.html',
  styleUrls: ['./cookies-policy.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CookiesPolicyComponent implements OnInit {
  title = 'Cookies Policy | Opinionest';
  description='When you visit our website, a web server sends a cookie to your system or mobile device to store and track valuable information to experience more efficient. Accept the cookie policy now!';
  keywords=' ';

  constructor(private seoService:ExtraActivitiesComponent,private titleService: Title,private meta: Meta) { }

  ngOnInit() {
    this.titleService.setTitle(this.title);
    this.meta.addTag({name: 'keywords', content: this.keywords});
    this.meta.addTag({name: 'description', content: this.description});
    this.meta.addTag({name: 'og:title', content: this.title});
    this.meta.addTag({name: 'og:site_name', content: 'Opinionest'});
    this.meta.addTag({name: 'og:url', content: 'https://opinionest.com/'});
    this.meta.addTag({name: 'og:description', content:this.description});
    this.meta.addTag({name: 'og:type', content: 'website'});
    this.meta.addTag({name: 'og:image', content: 'https://opinionest.com/assets/images/logo-color.png'});
    this.seoService.createLinkForCanonicalURL();

  }

}
