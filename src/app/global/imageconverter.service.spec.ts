import { TestBed } from '@angular/core/testing';

import { ImageconverterService } from './imageconverter.service';

describe('ImageconverterService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ImageconverterService = TestBed.get(ImageconverterService);
    expect(service).toBeTruthy();
  });
});
