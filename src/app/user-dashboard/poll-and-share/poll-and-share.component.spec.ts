import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PollAndShareComponent } from './poll-and-share.component';

describe('PollAndShareComponent', () => {
  let component: PollAndShareComponent;
  let fixture: ComponentFixture<PollAndShareComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PollAndShareComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PollAndShareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
