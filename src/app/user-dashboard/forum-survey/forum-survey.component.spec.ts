import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ForumSurveyComponent } from './forum-survey.component';

describe('ForumSurveyComponent', () => {
  let component: ForumSurveyComponent;
  let fixture: ComponentFixture<ForumSurveyComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ForumSurveyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForumSurveyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
