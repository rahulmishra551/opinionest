import { Component, OnInit,ViewEncapsulation } from '@angular/core';
import { HttpServiceService } from 'src/app/Services/common/http-service.service';
import { NavigationEnd, Router, ActivatedRoute } from '@angular/router';
import { FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-optional-questions',
  templateUrl: './optional-questions.component.html',
  styleUrls: ['./optional-questions.component.css']
})
export class OptionalQuestionsComponent implements OnInit {
  public form: FormGroup;
  unsubcribe: any
  count:number=0;
  mySubscription: any;
  ProfilePercentage: any;
  NoQuestion: boolean;
  
  constructor(private router: Router, private _HttpServiceService:HttpServiceService,private _snackBar:MatSnackBar,
    private route: ActivatedRoute) {
    this.getQuestions();
    this.route.params.subscribe( params => console.log(params) );
    this.router.routeReuseStrategy.shouldReuseRoute = function(){
      return false;
};
this.router.events.subscribe((evt) => {
  if (evt instanceof NavigationEnd) {
     this.router.navigated = false;
     window.scrollTo(0, 90);
  }
});
  }
  ngOnInit() { 
    this.getDashboardData();
  }
  public fields: any[] = [];
  onUpload(e) { }
  
  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }
  QuestionData:any;
  getQuestions()
  {
    const url="/api/QuestionAnswer/GetQuestion?MandatoryType=false"
    this._HttpServiceService.Get(url).subscribe(response => {
      if(response.data.answer==null)
      {
        this.NoQuestion=true;
      }
  if (response.data.question.type==='dropdown') {
    response.data.question.type='dropdown'
  }else if(response.data.question.type==='radiobutton'||response.data.question.type==='matrixradiobutton')
  {
    response.data.question.type='radio'
  }else if(response.data.question.type==='multiselect')
  {
    response.data.question.type='multiselect'
  }else if(response.data.question.type==='checkbox' || response.data.question.type==='matrixcheckBox')
  {
    response.data.question.type='checkbox'
  }
  else
  {
    response.data.question.type='text'
  }
  this.QuestionData=response.data.question;
  this.fields=[
    {
      type: response.data.question.type,
      name: 'key',
      label: response.data.question.profileQuestion,
      value: '',
      required: true,
      options: response.data.answer.map(e => {
        return {
          key: e.profileAnswerId,
          label: e.profileAnswer
        };
      })
    }]
      });
     
      this.fields=[ {
        type: '',
        name: 'key',
        label: '',
        value: '',
        required: true,
      }
    ]
  }
  answerId:any=0;
  SaveAnswer(x:any) { 
    this.answerId=x.key;
    const url = '/api/QuestionAnswer/SaveQuestionAnswer';
    this._HttpServiceService.Post(url, {
    profileQuestionId: this.QuestionData.profileQuestionId,
    profileAnswerId:String(x.key),
    profileAnswer:String(x.key),
    gpcid: this.QuestionData.gpcid,
    mandatory: true
  }).subscribe(response => {
    if(response.messageCode==1)
    {    
      this.getQuestions();
      this.openSnackBar(response.message, "Success");
      this.getDashboardData();
      x=null;
  }
  else
  {
    this.openSnackBar(response.message, "Alert");
  }
  });
   
  }
  getDashboardData()
  {
    const url = '/api/DashBoard/DashBoard';
    this._HttpServiceService.Get(url).subscribe(response => {
    
      this.ProfilePercentage=response.data.profileComplete;
      
    });
  }
}
