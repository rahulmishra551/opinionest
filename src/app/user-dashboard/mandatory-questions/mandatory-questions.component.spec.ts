import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MandatoryQuestionsComponent } from './mandatory-questions.component';

describe('MandatoryQuestionsComponent', () => {
  let component: MandatoryQuestionsComponent;
  let fixture: ComponentFixture<MandatoryQuestionsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MandatoryQuestionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MandatoryQuestionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
