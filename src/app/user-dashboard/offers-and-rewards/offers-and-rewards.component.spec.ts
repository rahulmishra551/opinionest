import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { OffersAndRewardsComponent } from './offers-and-rewards.component';

describe('OffersAndRewardsComponent', () => {
  let component: OffersAndRewardsComponent;
  let fixture: ComponentFixture<OffersAndRewardsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ OffersAndRewardsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OffersAndRewardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
