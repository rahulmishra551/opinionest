import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { RedeemOfferComponent } from './redeem-offer.component';

describe('RedeemOfferComponent', () => {
  let component: RedeemOfferComponent;
  let fixture: ComponentFixture<RedeemOfferComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ RedeemOfferComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RedeemOfferComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
