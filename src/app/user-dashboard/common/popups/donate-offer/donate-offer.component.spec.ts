import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DonateOfferComponent } from './donate-offer.component';

describe('DonateOfferComponent', () => {
  let component: DonateOfferComponent;
  let fixture: ComponentFixture<DonateOfferComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DonateOfferComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DonateOfferComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
