import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { OfferThankYouComponent } from './offer-thank-you.component';

describe('OfferThankYouComponent', () => {
  let component: OfferThankYouComponent;
  let fixture: ComponentFixture<OfferThankYouComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ OfferThankYouComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OfferThankYouComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
