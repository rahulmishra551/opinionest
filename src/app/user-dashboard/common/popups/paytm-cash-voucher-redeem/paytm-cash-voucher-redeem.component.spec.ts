import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PaytmCashVoucherRedeemComponent } from './paytm-cash-voucher-redeem.component';

describe('PaytmCashVoucherRedeemComponent', () => {
  let component: PaytmCashVoucherRedeemComponent;
  let fixture: ComponentFixture<PaytmCashVoucherRedeemComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PaytmCashVoucherRedeemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaytmCashVoucherRedeemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
