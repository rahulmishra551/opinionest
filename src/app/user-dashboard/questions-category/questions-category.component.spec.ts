import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { QuestionsCategoryComponent } from './questions-category.component';

describe('QuestionsCategoryComponent', () => {
  let component: QuestionsCategoryComponent;
  let fixture: ComponentFixture<QuestionsCategoryComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ QuestionsCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionsCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
