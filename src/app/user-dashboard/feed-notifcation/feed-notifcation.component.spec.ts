import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { FeedNotifcationComponent } from './feed-notifcation.component';

describe('FeedNotifcationComponent', () => {
  let component: FeedNotifcationComponent;
  let fixture: ComponentFixture<FeedNotifcationComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ FeedNotifcationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeedNotifcationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
