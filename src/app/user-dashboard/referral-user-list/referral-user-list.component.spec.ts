import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ReferralUserListComponent } from './referral-user-list.component';

describe('ReferralUserListComponent', () => {
  let component: ReferralUserListComponent;
  let fixture: ComponentFixture<ReferralUserListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ReferralUserListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReferralUserListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
