import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ReferAndEarnComponent } from './refer-and-earn.component';

describe('ReferAndEarnComponent', () => {
  let component: ReferAndEarnComponent;
  let fixture: ComponentFixture<ReferAndEarnComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ReferAndEarnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReferAndEarnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
