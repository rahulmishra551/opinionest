import { library } from '@fortawesome/fontawesome-svg-core';
import { faFacebookF, faFacebookMessenger, faGithub, faGooglePlusG, faLine, faLinkedinIn, faMix, faPinterestP, faRedditAlien, faTelegramPlane, faTumblr, faTwitter, faVk, faWhatsapp, faXing } from '@fortawesome/free-brands-svg-icons';

const icons = [faGithub,
  faFacebookF, faTwitter, faLinkedinIn, faGooglePlusG, faPinterestP, faRedditAlien, faTumblr,
  faWhatsapp, faVk, faFacebookMessenger, faTelegramPlane, faMix, faXing, faLine
];

library.add(...icons);